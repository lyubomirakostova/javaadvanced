package tests;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class Tests1 {
    @BeforeClass
    public static void beforeClass() {
        System.out.println("Tests in Tests1 class are running.");
    }

    @Test
    public void test11() {
        Assert.assertTrue(true);
    }

    @Test
    public void test12() {
        Assert.assertNull(null);
    }

    @Test
    public void test13() {
        Assert.assertFalse(false);
    }

    @Test
    public void test14() {
        Assert.assertEquals(15L, 15L);
    }
    @Test
    public void test15() {
        Assert.assertEquals(5, 55);
    }
    @Test
    public void test16() {
        Assert.assertEquals("bar", "barr");
    }
}