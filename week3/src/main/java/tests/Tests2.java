package tests;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class Tests2 {
    @BeforeClass
    public static void beforeClass() {
        System.out.println("Tests in Tests2 class are running.");
    }

    @Test
    public void test21() {
        Assert.assertTrue(true);
    }

    @Test
    public void test22() {
        Assert.assertNull(null);
    }

    @Test
    public void test23() {
        Assert.assertFalse(false);
    }

    @Test
    public void test24() {
        Assert.assertEquals(15L, 15L);
    }

    @Test
    public void test25() {
        Assert.assertEquals("foo", "bar");
    }
}
