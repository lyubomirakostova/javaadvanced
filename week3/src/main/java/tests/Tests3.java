package tests;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class Tests3 {
    @BeforeClass
    public static void beforeClass() {
        System.out.println("Tests in Tests3 class are running.");
    }

    @Test
    public void test31() {
        Assert.assertTrue(true);
    }

    @Test
    public void test32() {
        Assert.assertNull(null);
    }

    @Test
    public void test33() {
        Assert.assertEquals("foo","foo");
    }

}
