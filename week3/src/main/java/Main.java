import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import tests.Tests1;
import tests.Tests2;
import tests.Tests3;

import java.lang.reflect.Method;
import java.util.*;

public class Main {
    static List<String> historyOfExecutedTests = new ArrayList<String>();
    static Map<String, Integer> timesOfExecutingSuccess = new HashMap<String, Integer>();
    static Map<String, Integer> timesOfExecutingFailure = new HashMap<String, Integer>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String number;
        String testClass;
        while (true) {
            System.out.println("Enter a number from 1 to 4:");
            System.out.println("Enter \'end\' to exit.");
            number = scanner.nextLine();

            if (number.equalsIgnoreCase("END")) {
                break;
            }
            if (number.equals("1")) {
                System.out.println("Which test class do you want to execute?");
                testClass = scanner.nextLine();
                if (testClass.equalsIgnoreCase("Tests1")) {
                    execute(Tests1.class);
                } else if (testClass.equalsIgnoreCase("Tests2")) {
                    execute(Tests2.class);
                } else if (testClass.equalsIgnoreCase("Tests3")) {
                    execute(Tests3.class);
                } else {
                    System.out.println("Not a valid name for a test class.");
                }

            }
            if (number.equals("2")) {
                System.out.println("History of executed methods:");
                for (int i = 0; i < historyOfExecutedTests.size(); i++) {
                    System.out.println(historyOfExecutedTests.get(i));
                }
            }
            if (number.equals("3")) {
                System.out.println("Not implemented yet");
            }
            if (number.equals("3")) {
                System.out.println("Not implemented yet");
            }

        }


    }

    public static void execute(Class testClass) {
        Result result = JUnitCore.runClasses(testClass);
        for (Method method : testClass.getDeclaredMethods()) {
            if (!method.getName().equals("beforeClass")) {
                historyOfExecutedTests.add(method.getName());
            }
            if (!timesOfExecutingSuccess.containsKey(method.getName())) {
                timesOfExecutingSuccess.put(method.getName(), 0);

            } else {
                timesOfExecutingSuccess.put(method.getName(),
                        timesOfExecutingSuccess.get(method.getName() + 1));
            }
        }
        System.out.println("Success: " + (result.getRunCount() - result.getFailureCount()));
        System.out.println("Failure: " + result.getFailureCount());
        System.out.println("Total: " + result.getRunCount());
    }
}
